# MOC Adventure

## Genre: RPG, Puzzle

## Staff: Yingzhu Xiong, Yiming Wang, Zhenglin Yu, Han Chen, Jingyang Ye


## Spoiler: 

Drawing inspiration from the movie WALL-E, we have designed an open-world escape game set in the near future, as humanity begins to colonize Mars. Players are immersed in the role of MOC(Mars Orb Collector), a small 3D-printed robot tasked with gathering energy orbs scattered across the planet.

Unfortunately, MOC becomes separated from its work unit and finds itself trapped in an unknown location. The player's objective is to guide MOC back to its team while collecting as many energy orbs as possible along the way. To do so, MOC must navigate through a series of interconnected environments filled with intricate puzzles, locked doors, and hidden passageways. Players will be challenged to think critically as MOC uncovers the solutions to various puzzles, activating switches to operate elevators, fans, and unlocking doors. To assist in the quest, MOC possesses a unique ability: it can create a single duplicate of itself at its starting point. This duplicate can be employed strategically to overcome obstacles and progress through the adventure.




